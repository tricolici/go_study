package main

import "fmt"
import "regexp"

// Sample 1
func test_match1(rx string, ms string) {
  var r, err = regexp.Compile(rx)
  if (err != nil) {
    fmt.Printf("Regex cannot compile: '%s'\n", rx)
    return
  }
  fmt.Printf("Compilation success '%s'.", rx)
  var result = r.MatchString(ms)
  fmt.Printf("match('%s') is '%t'\n", ms, result)
}

// Sample 2
func test_sample2() {
  r, _ := regexp.Compile("p([a-z]+)ch")
  fmt.Print("MatchString:")
  fmt.Println(r.MatchString("peach"))

  fmt.Print("FindString:")
  fmt.Println(r.FindString("qpeach punch"))

  fmt.Print("FindStringIndex:")
  fmt.Println(r.FindStringIndex("qpeach punch"))

  fmt.Print("FindStringSubmatch")
  fmt.Println(r.FindStringSubmatch("qpeeeach punch"))
}


// Main Method
func main() {
  test_match1("p([a-z]+)ch", "zuzu")
  test_match1("p([a-z]+)ch", "peaaach")
  test_sample2()
}

