package main

import "fmt"
import "net/http"
import "io/ioutil"
//import "reflect"

func test_get() {
  r, err := http.Get("http://mail.md/")
  if (err != nil) {
    fmt.Println("http get failed: ", err)
  } else {
    //fmt.Println(reflect.TypeOf(resp))
    fmt.Println("status", r.StatusCode)
    defer r.Body.Close() // like 'finally'
    bodyBinary, _ := ioutil.ReadAll(r.Body)
    bodyString := string(bodyBinary)
    fmt.Println(bodyString[0:50])
  }
}

func main() {
  test_get()
}

